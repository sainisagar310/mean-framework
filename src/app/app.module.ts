import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { indexRoutes } from './app.route';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import 'hammerjs';

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

// Service
import { AuthService } from './user/auth.service';

// Modules
import { UserModule } from './user/user.module';

// Guards
import { UserGuard } from './user/user.guard';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
	BrowserModule,
	UserModule.forRoot(),
	indexRoutes,
	BrowserAnimationsModule,
	MaterialModule,
	FlexLayoutModule
  ],
  providers: [AuthService, UserGuard],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
