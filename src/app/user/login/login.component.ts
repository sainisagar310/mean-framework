import { Component } from '@angular/core';
import {MdDialog, MdDialogRef, MdSnackBar} from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { UserService } from '../user.services';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  styleUrls: [ './login.component.scss' ],
  templateUrl: './login.component.html'
})
export class LoginComponent {

	form: FormGroup;
	email = new FormControl('', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/)]);
	password = new FormControl('', [Validators.required]);
	remember = new FormControl('', []);

	constructor(
	private formBuilder: FormBuilder,
	private router: Router,
	private userService: UserService,
	private auth: AuthService,
	public snackBar:MdSnackBar,
  public dialog:MdDialog) {
		if(localStorage.getItem('token')) this.router.navigate(['/']);
		this.form = this.formBuilder.group({
		  email: this.email,
		  password: this.password,
		  remember: this.remember
		});
	}

	/**
	 * @Login form submission.
	*/
	public login(): void {
		this.userService.login(this.form.value).subscribe(
			res => {
				this.auth.setCurrentUser(res.json());
				this.router.navigate(['/']);
			},
			error => {
				const err = error.json();
				if(err.field) this[err.field].setErrors({wrong: true});
				else this.snackBar.open(("string" == typeof err.message) ? err.message : "Something went wrong. Please try again.", '', {duration:2000});
			}
		);
	}

  requestForgotPassword(): void{
    let dialogRef = this.dialog.open(RequestForgotDialog, {
      data: this.form.value.email,
      width:'500px'
    });
  }

}

import { RequestForgotDialog } from "../request-forgot/request-forgot.dialog";
