"use strict";
var express = require('express');
var jwt = require("jsonwebtoken");
var userModel = require("../models/user.model");
var bcrypt = require("bcryptjs");

var mailController = require('./mail.controller');
var mailCtrl = new mailController.default();

function userController(){
	var model = userModel.default;

	this.register = function(req, res) {
		req.body.emailVerifyKey = Math.abs(Date.now());
		req.body.photograph = 'files/no-avatar.jpg';
		new model(req.body).save(function (err, user) {
			if (err) return res.status(400).json(err);
			var token = jwt.sign({ user: user }, process.env.SECRET_TOKEN);
			res.json({ type: true, token: token, user: user });
			console.log(process.env.BASE_URL+'user/email-verification/'+user._id+'/'+req.body.emailVerifyKey);
			mailCtrl.sendMail({
				email: req.body.email,
				subject: "Email Verification",
				html: "Welcome "+user.name+" <a href='"+process.env.BASE_URL+'user/email-verification/'+user._id+'/'+req.body.emailVerifyKey+"'>Click to Verify your email</a>"
			});
		});
	}

	this.login = function(req, res){
		model.findOne({email:req.body.email}, function(err, user){
			if(err) return res.status(400).send(err);
			if(!user) return res.status(400).json({field: 'email'});
			user.comparePassword(req.body.password, function(done){
				if(!done) return res.status(400).json({field: 'password'});
				var token = jwt.sign({ user: user }, process.env.SECRET_TOKEN);
				res.json({token:token, user:user});
			});
		})
	}
	this.token = function(req, res){
		jwt.verify(req.body.token, process.env.SECRET_TOKEN, function(err, decode){
			if(err) return res.status(400).send('Invalid Token');
			res.json({token:req.body.token, user:decode.user});
		});
	}

	this.tokenReset = function(req, res) {
		jwt.verify(req.body.token, process.env.SECRET_TOKEN, function(err, decode){
			if(err) return res.status(400).send('Invalid Token');
			model.findOne({_id:decode.user._id}, function(err, user){
				if(err) return res.status(400).send(err);
				var token = jwt.sign({ user: user }, process.env.SECRET_TOKEN);
				res.json({token: token, user: user});
			});
		});
	}
	this.update = function(req, res) {
		jwt.verify(req.body.token, process.env.SECRET_TOKEN, function(err, decode){
			if(err) return res.status(400).json('Invalid Token');
			model.findOneAndUpdate({_id:decode.user._id}, req.body.user, {}, function(err, d){
				if(err) return res.status(400).send("User can't be update.");
				res.json(d);
			})
			
		});
	}

	this.emailVerify = function(req, res) {
		model.findOne({_id:req.body.id}, function(err, user){
			if(err) return res.status(400).send('User does not exists.');
			if(!user) return res.status(400).send('User does not exists.');
			user.emailverification(req.body.code, function(done, msg){
				if(!done) return res.status(400).send(msg);
				model.findOneAndUpdate({_id:req.body.id}, {emailVerify:1, emailVerifyKey:0}, {}, function(err, d){
					if(err) return res.status(400).send('Email has not verified.');
					res.send(msg);
				});
			});
		});
	}
	this.requestForgot = function(req, res) {
		const emailVerifyKey = Math.abs(Date.now());
		model.findOneAndUpdate({email:req.body.email}, {emailVerifyKey:emailVerifyKey}, {}, function(error, user){
			if(error) return res.status(400).send('User does not exists.');
			if(!user) return res.status(400).send('This email is not registerd with us.');
			console.log(process.env.BASE_URL+'user/forgot-password/'+user._id+'/'+emailVerifyKey);
			mailCtrl.sendMail({
				email: req.body.email,
				subject: "Forgot Password",
				html: "Dear "+user.name+", <a href='"+process.env.BASE_URL+'user/forgot-password/'+user._id+'/'+emailVerifyKey+"'>Click to forgot reset your password.</a>"
			}, function(err, sent) {
				if(err) return res.status(400).send('Mail not sent.');
				res.send("We have sent you a url to forgot your password in your mail.");
			});
		});
	}
	this.forgotPassword = function(req, res) {
		model.findOne({_id:req.body.id}, function(err, user){
			if(err) return res.status(400).send('This link is wrong.');
			if(!user) return res.status(400).send('This link is wrong.');
			user.checkCode(req.body.code, function(err){
				if(err) return res.status(400).send("This link is wrong.");
				model.findOne({_id:req.body.id}, function(err, u){
					if(err) return res.status(400).send("This link is wrong.");
					u.set({emailVerifyKey:0, password:req.body.password});
					u.save(function(err, user){
						if(err) return res.status(400).send('Password has not changed. Please try again.');
						res.send('Password has changed.');
					});
				});
			});
		});
	}

}

exports.default = userController;
