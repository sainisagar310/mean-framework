import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
	
	private headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
	private options = new RequestOptions({ headers: this.headers });

	constructor(private http: Http) {}
	
	register(user): Observable<any> {
		return this.http.post('/api/user/register', JSON.stringify(user), this.options);
	}
	
	login(data): Observable<any>{
		return this.http.post('/api/user/login', JSON.stringify(data), this.options);
	}
	token(token): Observable<any>{
		return this.http.post('/api/user/token', {token:token}, this.options);
	}
	tokenReset(token): Observable<any>{
		return this.http.post('/api/user/token-reset', {token:token}, this.options);
	}
	userUpdate(data): Observable<any>{
		return this.http.post('/api/user/update', data, this.options);
	}
	emailVerify(d): Observable<any> {
		return this.http.post('/api/user/email-verify', d, this.options);
	}
	requestForgot(d): Observable<any> {
		return this.http.post('/api/user/request-forgot', d, this.options);
	}
	forgotPassword(d): Observable<any> {
		return this.http.post('/api/user/forgot-password', d, this.options);
	}
	
	// Image Upload
	imageUpload(data): Observable<any> {
		return this.http.post('/api/image-upload', data, this.options);
	}
	
}