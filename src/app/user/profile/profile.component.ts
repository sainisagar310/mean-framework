import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { UserService } from '../user.services';
import {MdDialog, MdDialogRef, MdSnackBar} from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
	nameEdit = false;
	usernameEdit = false;
	phoneEdit = false;
	emailEdit = false;
	genderEdit = false;
	imgLoading:boolean;
	form: FormGroup;
	editname = new FormControl();
	username = new FormControl();
	phone = new FormControl();
	email = new FormControl();
	gender = new FormControl();
  constructor(public data:AuthService, private formBuilder: FormBuilder, private userService:UserService, public snackBar: MdSnackBar, public dialog:MdDialog) { }
	
  ngOnInit() {
		this.form = this.formBuilder.group({
		  editname: this.editname,
		  username: this.username,
		  phone: this.phone,
		  email: this.email,
		  gender: this.gender
		});
		
  }
	userUpdate(data, field) {
		if(!data[field]) return this.snackBar.open('The field not updated.', '', {duration:2000});
		this.userService.userUpdate({token:localStorage.getItem('token'), user:data}).subscribe(
			response => {
				this.data.tokenReset();
				this[field+'Edit'] = false;
			},
			err => {
				this.snackBar.open(err._body, '', {duration:2000});
			}
		);		
	}
	
	imgChange() {
		let dialogRef = this.dialog.open(PhotographDialog, { width: '600px' });		
		dialogRef.afterClosed().subscribe(res => {
			if(!res) return false;
			this.imgLoading = true;
			var img = res.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
			this.userService.imageUpload({image:img, path:'users/'}).subscribe(res => {
				this.userService.userUpdate({token:localStorage.getItem('token'), user:{photograph:res.json().path}}).subscribe(
					res => {
						this.data.tokenReset();
						this.imgLoading = false;
					},
					err => {
						this.imgLoading = false;
						this.snackBar.open(err._body, '', {duration:2000});
					}
				);				
			},
			err => {
				this.imgLoading = false;
				this.snackBar.open(err._body, '', {duration:2000});
			});			
    });
	}
	
}

import { PhotographDialog } from './dialog-photograph/photograph.dialog';