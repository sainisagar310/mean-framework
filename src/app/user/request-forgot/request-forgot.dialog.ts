import { Component, Inject } from '@angular/core';
import { MdDialogRef, MdSnackBar, MD_DIALOG_DATA} from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { UserService } from '../user.services';

@Component({
  selector: 'app-request-forgot-dialog',
  templateUrl: './request-forgot.dialog.html'
})
export class RequestForgotDialog {
	form: FormGroup;
	email = new FormControl('', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/)]);
	formSubmitting:boolean;
  result:boolean;
  message:any;
  constructor(
	private formBuilder: FormBuilder,
	public mdSnackBar:MdSnackBar,
	public dialogRef: MdDialogRef<RequestForgotDialog>,
  @Inject(MD_DIALOG_DATA) public data: any,
  private userService:UserService) {
    console.log(this.data);
		this.form = this.formBuilder.group({
			email: this.email
		});
    this.form.controls['email'].setValue(data);
	}

	save(){
		this.formSubmitting = true;
    this.userService.requestForgot({email:this.form.value.email}).subscribe(
      res => {
        this.formSubmitting = false;
        this.result = true;
        this.message = res._body;
      },
      err => {
        this.formSubmitting = false;
        this.result = false;
        this.message = err._body;
      }
    )
	}
}
