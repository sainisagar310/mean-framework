var express = require('express');
var router = express.Router();

//Routes
var userRoutes = require('./user.route');

// controllers
var indexController = require('../controllers/index.controller');

function indexRoutes(app){
	var indexCtrl = new indexController.default();
	router.route('/').get(indexCtrl.home);
	router.route('/image-upload').post(indexCtrl.imageUpload);
	router.use('/user', userRoutes.default);
	app.use('/api', router);
}

exports.default = indexRoutes;