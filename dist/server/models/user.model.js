"use strict";
var bcrypt = require("bcryptjs");
var mongoose = require("mongoose");
var userSchema = new mongoose.Schema({
	name: { type: String, required: true },
	email: { type: String, match: [/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/, 'Please enter a valid email.'], required:true, unique: true},
	emailVerify: Boolean,
	emailVerifyKey: Number,
	phone: { type: String, match: [/^[1-9][0-9]{9}$/, 'Please enter a valid mobile number.']},
	gender: Number,
	username: { type: String, unique: true },
	hashed_password: { type: String },
	dob: { type: Date },
	photograph: String,
	status: { type: Boolean, default: true },
	deleted: Boolean,
	createdAt: { type: Date, default: new Date() }
});

userSchema.virtual('password').set(function (password) {
    this.hashed_password = bcrypt.hashSync(password, 10);
});

userSchema.pre('save', function (next) {
    this.username = this.name.replace(/[^A-Z0-9]/ig, "").toLowerCase() + Date.now();
    next();
});
userSchema.methods.comparePassword = function (candidatePassword, callback) {
    if(bcrypt.compareSync(candidatePassword, this.hashed_password)){
		callback(true);
	} else {
		callback(false);
	};
};
userSchema.methods.emailverification = function (code, callback) {
	if(this.emailVerify) return callback(false, "Your email is already verified.");
	if(parseInt(code) === Math.abs(this.emailVerifyKey)) return callback(true, "Email verified successfully.");
	callback(false, "Wrong verification link.");
};
userSchema.methods.checkCode = function (code, callback) {
	if(parseInt(code) === Math.abs(this.emailVerifyKey)) return callback(false);
	callback(true);
};

userSchema.set('toJSON', {
    transform: function (doc, ret, options) {
			delete ret.hashed_password;
			delete ret.createdAt;
			delete ret.emailVerify;
			delete ret.deleted;
			delete ret.status;
			if(ret.gender == 1) ret.gender = 'Male';
			else ret.gender = 'Female';
			return ret;
    }
});

var User = mongoose.model('User', userSchema);
exports.default = User;
