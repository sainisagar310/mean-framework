import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { UserService } from '../user.services';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
	verificationCode:Number;
	id:String;

  formResult:boolean;
  formResultMessage:string;
	form: FormGroup;
	password = new FormControl('', [Validators.required]);
	confirm_password = new FormControl('', [Validators.required]);

  constructor(private route: ActivatedRoute, private userService:UserService, private formBuilder: FormBuilder) {
		this.route.params.subscribe(params => {this.verificationCode = params.code; this.id = params.id});
		this.form = this.formBuilder.group({
			password: this.password,
			confirm_password: this.confirm_password
		});
	}

  ngOnInit() {
  }

	forgot(): void{
    if(this.form.value.password === this.form.value.confirm_password) {
      this.userService.forgotPassword({code:this.verificationCode, id:this.id, password:this.form.value.password}).subscribe(
        res => {
          this.formResult = true;
        },
        err => {
          this.formResult = false;
          this.formResultMessage = err._body;
        }
      )
    } else {
      this.confirm_password.setErrors({match: true});
    }
	}

}
