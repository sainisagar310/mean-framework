import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './user.services';

// Interface
import { UserInterface } from './user.interface';

@Injectable()
export class AuthService {
	loggedIn = false;
	user:UserInterface;
	
	
  constructor(
	private router: Router,
	private userService: UserService) {
		if(localStorage.getItem('token')) {
			this.userService.token(localStorage.getItem('token')).subscribe(res => this.setCurrentUser(res.json()), err => {this.logout()});
		}		
	}
	
	token(){
		this.userService.token(localStorage.getItem('token')).subscribe(res => this.setCurrentUser(res.json()), err => this.logout());
	}
	
	tokenReset() {
		this.userService.tokenReset(localStorage.getItem('token')).subscribe(res => this.setCurrentUser(res.json()), err => this.logout());
	}
	
	setCurrentUser(data){
		localStorage.setItem('token', data.token);
		this.loggedIn = true;
		this.user = data.user;
	}
	
	logout() {
		localStorage.removeItem('token');
		this.loggedIn = false;
		this.user = null;
		this.router.navigate(['/user/login']);
	}
	
	getUser() {
		return this.user;
	}

}
