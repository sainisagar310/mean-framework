export interface UserInterface {
	_id: String,
	name: String,
	email: String,
	username: String,
	phone: String,
	dob:String,
	gender: String,
	/* roles: Array<string>, */
}
