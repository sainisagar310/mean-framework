"use strict";
var mongoose = require("mongoose");
var courseSchema = new mongoose.Schema({
    name: {type: String, required:true},
    length: {type: String, required:true},
		fee: {type:Number},
		books: [{type:mongoose.Schema.Types.ObjectId, ref:"School.books"}],
    deleted: Number,
		status: Number,
    createdAt: { type: Date, default: new Date() }
});
exports.default = courseSchema;