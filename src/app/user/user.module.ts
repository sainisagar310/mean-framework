import { CommonModule  } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule }  from '@angular/http';
import { MaterialModule, MdDatepickerModule, MdNativeDateModule, MdInputModule } from '@angular/material';
import { ImageCropperModule } from 'ng2-img-cropper';
import { FlexLayoutModule } from '@angular/flex-layout';

import { routes } from './user.routes';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { LogoutComponent } from './logout/logout.component';

import { UserService } from "./user.services";
import { ProfileComponent } from './profile/profile.component';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';

import { RequestForgotDialog } from './request-forgot/request-forgot.dialog';
import { PhotographDialog } from './profile/dialog-photograph/photograph.dialog';


@NgModule({
  declarations: [
    /**
     * Components / Directives/ Pipes
     */
	LoginComponent,
	RegisterComponent,
	LogoutComponent,
	ProfileComponent,
	EmailVerificationComponent,
	ForgotPasswordComponent,
  RequestForgotDialog,
	PhotographDialog
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    RouterModule,
    HttpModule,
    MaterialModule,
	MdInputModule,
	MdDatepickerModule,
	MdNativeDateModule,
	FlexLayoutModule,
	ImageCropperModule
  ],
  providers: [ UserService ],
  exports: [LoginComponent, RegisterComponent, LogoutComponent],
  entryComponents:[RequestForgotDialog, PhotographDialog]

})
export class UserModule {
  //public static routes = routes;
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: UserModule,
      providers: [UserService]
    }
  }
}
