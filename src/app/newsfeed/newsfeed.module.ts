import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsfeedRoutingModule } from './newsfeed-routing.module';

@NgModule({
  imports: [
    CommonModule,
    NewsfeedRoutingModule
  ],
  declarations: []
})
export class NewsfeedModule { }
