import { MyMeanPage } from './app.po';

describe('my-mean App', () => {
  let page: MyMeanPage;

  beforeEach(() => {
    page = new MyMeanPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
