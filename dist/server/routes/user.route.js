"use strict";
var express = require("express");
var router = express.Router();
var userController = require('../controllers/user.controller');
var userCtrl = new userController.default();

//Routes
router.route('/register').post(userCtrl.register);
router.route('/login').post(userCtrl.login);
router.route('/token').post(userCtrl.token);
router.route('/token-reset').post(userCtrl.tokenReset);
router.route('/update').post(userCtrl.update);
router.route('/email-verify').post(userCtrl.emailVerify);
router.route('/request-forgot').post(userCtrl.requestForgot);
router.route('/forgot-password').post(userCtrl.forgotPassword);

exports.default = router;