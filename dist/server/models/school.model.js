"use strict";
var mongoose = require("mongoose");

var userModel = require("school.user.model");
var courseModel = require("school.course.model");
var bookModel = require("school.book.model");
var libraryStuffModel = require("school.library-stuff.model");

var schoolSchema = new mongoose.Schema({
    name: {type: String, required:true},
		email: {type:String, match: [/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/, 'Please enter a valid email.'], required:true},
		contact: [{name:String, value:String}],
		website: String,
		address: {type:String, required:true},
		city: {type:String, required:true},
		state: {type:String, required:true},
		country: {type:String, required:true},
		currency: {type:String, match:['INR', 'USD', 'AUD']},
		mailSetup: {
			host: String,
			ssl: String,
			username: String,
			password: String,
			port: Number,
		},
		users: [userModel.default],
		courses: [courseModel.default],
		books:[bookModel.default],
		libraryStuff: [libraryStuffModel.default],
    deleted: Number,
		status: {type:Number, default:1},
    createdAt: { type: Date, default: new Date() }
});
var School = mongoose.model('School', schoolSchema);
exports.default = School;