import { Component, ViewChild } from '@angular/core';
import {ImageCropperComponent, CropperSettings} from 'ng2-img-cropper';
import { MdDialogRef, MdSnackBar} from '@angular/material';
@Component({
  selector: 'app-photograph-dialog',
  templateUrl: './photograph.dialog.html',
	styleUrls: ['./photograph.dialog.scss'],
})
export class PhotographDialog {
	@ViewChild('cropper', undefined)
	cropper:ImageCropperComponent;	
	cropperSettings: CropperSettings;
	model: any;
  constructor(public dialogRef: MdDialogRef<PhotographDialog>, public snackBar:MdSnackBar) {
		this.cropperSettings = new CropperSettings();
		this.cropperSettings.width = 400;
		this.cropperSettings.height = 400;
		this.cropperSettings.croppedWidth = 400;
		this.cropperSettings.croppedHeight = 400;
		this.cropperSettings.canvasWidth = 400;
		this.cropperSettings.canvasHeight = 400;
		this.cropperSettings.minWidth = 400;
		this.cropperSettings.minHeight = 400;
		this.cropperSettings.noFileInput = true;
		this.model = {photograph:{}};
	}
	
	upload($event) {
		var image:any = new Image();
    var file:File = $event.target.files[0];
    var myReader:FileReader = new FileReader();
    var that = this;
    myReader.onloadend = function (loadEvent:any) {
        image.src = loadEvent.target.result;
        that.cropper.setImage(image);
    };
    myReader.readAsDataURL(file);
	}
	
	imageUpload(): void {
		if(this.model.photograph.image) {
			this.dialogRef.close(this.model.photograph.image);
		} else {
			this.snackBar.open('You have not selected a photograph.', '', {duration:2000});
		}
	}

}
