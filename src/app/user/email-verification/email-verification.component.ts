import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { UserService } from '../user.services';

@Component({
  selector: 'app-email-verification',
  templateUrl: './email-verification.component.html',
  styleUrls: ['./email-verification.component.css']
})
export class EmailVerificationComponent implements OnInit {
	verificationCode:Number;
	id:String;
	verifying = true;
	msg:String;
	verification = false;
  constructor(private route: ActivatedRoute, private userService:UserService) {
		this.route.params.subscribe(params => {this.verificationCode = params.code; this.id = params.id});
		this.userService.emailVerify({code:this.verificationCode, id:this.id}).subscribe(
			res => {
				this.verifying = false;
				this.msg = res._body;
				this.verification = true;
			},
			error => {
				this.verifying = false;
				this.msg = error._body;
				
			}
		);
	}

  ngOnInit() {
  }

}
