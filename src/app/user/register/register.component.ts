import { Component } from '@angular/core';
import {MdSnackBar} from '@angular/material';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { UserService } from '../user.services';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-register',
  styleUrls: [ './register.component.scss' ],
  templateUrl: './register.component.html'
})
export class RegisterComponent {

	form: FormGroup;
	name = new FormControl('', [Validators.required]);
	email = new FormControl('', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/)]);
	gender = new FormControl('', [Validators.required]);
	password = new FormControl('', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/)]);
	
	constructor(
	private formBuilder: FormBuilder,
	private router: Router,
	private userService: UserService,
	private auth: AuthService,
	public snackBar:MdSnackBar) {
		this.form = this.formBuilder.group({
		  name: this.name,
		  email: this.email,
		  gender: this.gender,
		  password: this.password,
		});
	}
	
	ngOnInit(): void {}
	
	/**
	 * @user registration form submission.
	 * @login as well after the submission.
	*/
	public register(): void {		
		this.userService.register(this.form.value).subscribe(
			response => {
				const data = response.json();
				if(data.token) {
					this.auth.setCurrentUser(data);
					this.router.navigate(['/']);
				} else this.snackBar.open("Something went wrong. Please try again.", '', {duration:2000});
			},
			error => {
				const err = error.json();
				if(err.code == 11000) this.email.setErrors({notUnique: true});
				else this.snackBar.open(("string" == typeof err.message) ? err.message : "Something went wrong. Please try again.", '', {duration:2000});
			}
		);
	}
	
}