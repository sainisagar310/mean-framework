"use strict";
var mongoose = require("mongoose");
var mailSchema = new mongoose.Schema({
    host: String,
    ssl: String,
    username: String,
		password: String,
    port: Number,
});
exports.default = mailSchema;