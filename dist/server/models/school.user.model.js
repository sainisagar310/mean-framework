"use strict";
var mongoose = require("mongoose");
var userSchema = new mongoose.Schema({
		type: {type:String, match:['STUDENT', 'PARENT', 'TEACHER'], required:true},
    fname: {type: String, required:true},
    lname: {type: String},
		email: {type:String, match: [/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/, 'Please enter a valid email.']},
		dob: {type:String, required:true, match: [/^[0-9]{2}-[0-9]{2}-[0-9]{4}$/, 'Date of Birth is wrong.']},
		password: String,
		parent:{type:mongoose.Schema.Types.objectId, ref:"School.users"},
    deleted: Number,
		status: Number,
    createdAt: { type: Date, default: new Date() }
});
exports.default = userSchema;