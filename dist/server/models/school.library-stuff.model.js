"use strict";
var mongoose = require("mongoose");
var libraryStuffSchema = new mongoose.Schema({
    book: {type: mongoose.Schema.Types.ObjectId, ref:"School.books", required:true},
    quantity: {type: Number, required:true},
		cost: {type:Number},
    deleted: Number,
    createdAt: { type: Date, default: new Date() }
});
exports.default = libraryStuffSchema;