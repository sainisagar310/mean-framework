import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { LogoutComponent } from './logout/logout.component';
import { ProfileComponent } from './profile/profile.component';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
// Guards
import { UserGuard } from './user.guard';

export const routes = [
	{ path: 'login', component: LoginComponent },
	{ path: 'register', component: RegisterComponent },
	{ path: 'logout', component: LogoutComponent },
	{ path: 'profile', component: ProfileComponent },
	{ path: 'email-verification/:id/:code', component: EmailVerificationComponent },
	{ path: 'forgot-password/:id/:code', component: ForgotPasswordComponent }
];