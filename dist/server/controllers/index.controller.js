var fs = require('fs');
function indexController(){
	this.home = function(req, res){
		res.json({type:false});
	}
	this.imageUpload = function(req, res) {
		var filePath = 'files/'+req.body.path+Date.now()+'.png';
		fs.writeFile('dist/'+filePath, req.body.image, 'base64', function(err){
			if(err) res.status(400).send('File can not be write.');
			res.json({path:filePath});
		});
	}
}

exports.default = indexController;