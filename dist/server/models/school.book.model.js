"use strict";
var mongoose = require("mongoose");
var bookSchema = new mongoose.Schema({
    name: {type: String, required:true},
    writer: {type: String, required:true},
		value: {type:Number},
    deleted: Number,
    createdAt: { type: Date, default: new Date() }
});
exports.default = bookSchema;